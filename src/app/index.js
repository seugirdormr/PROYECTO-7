import React from 'react';
import Buttoms from'../buttoms'
import Input from'../input'

export default class App extends React.Component {

  constructor() {
    super()
    this.state = {
      currentInput: '',
      history: [
        {
        operation: '',
        result: 0,
        }
      ]
    }
  }

  selector(char) {
    const charSelector= ['/', '*', '-', '+', ',']
    for (let i = 0; i < charSelector.length; i++) {
      if (i == char) {
        return true
        i = charSelector + 1
      };
    }

  }

  // showHis(){
  //   return(
  //     <div>
  //       {this.history['operation']} = {this.history['result']}
  //     </div>
  //   )
  // }
  
  // showCurrentInput = (value) => {
  //   if (1 < this.state.currentInput.length + 1 && typeof(value) == 'number') {
  //     const change = [...this.state.inputChar]
  //     change[change.length] = (this.state.inputChar * 10) + value
  //     this.setState({
  //       inputChar: change
  //     });
  //   } else if ( typeof(value) == 'string' ) {
  //     const change = [...this.state.inputChar]
  //     change[change.length] = value
  //     change[change.length] = null
  //     this.setState({
  //       inputChar: change 
  //     });
  //   } else {
  //     this.setState({
  //       inputChar: value
  //     });
  //   }
        
  //   console.log(this.state.inputChar)
  // }


  handleClick = (value) => {
    const charSelector= ['/', '*', '-', '+', ',']
    if (typeof(value) == 'number' || charSelector.includes(value)) {
      const currentInput = this.state.currentInput;
      this.setState({
        currentInput : currentInput + value.toString()
      });
    } else {
      switch (value) {
        case '=':
          const replace = this.state.currentInput.replace(/,/gi, '.')
          const history = this.state.history
          console.log('variable')
          console.log(history)
          const currentInput = eval(replace).toString();
          this.setState({
            history: history.concat({
              operation: this.state.currentInput,
              result: eval(replace).toString().replace(/,/gi, '.')
            }),
            currentInput
          })
        console.log('State')
        console.log(this.state.history)
          break;
        
        case '←':
          const inputModify = this.state.currentInput;
          this.setState({
            currentInput : inputModify.slice(0,-1)
          });

          break;

        case 'C':
          const deleteInput = "";
          this.setState({
            currentInput: deleteInput 
          });

          break;
        
        case 'His':
          console.log(this.state.history)
          break;
        default:
          break;
      }
    }
  }

  render() {
    return (
      <div className='container m-auto'>
        <div className = 'row input'>
          <div className = 'col'>
          <Input currentInput={this.state.currentInput} history={this.state.history}/>
          </div>
        </div>
        <div className = 'row-fluid buttoms'>
          <Buttoms handleClick={this.handleClick}/>
        </div>
      </div>
    )
  }
}