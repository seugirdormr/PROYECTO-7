import React from 'react';

export default class Input extends React.Component {

  
  render() {
    return(
      <div className = 'input py-2 float-right pt-4'> {this.props.currentInput} </div>    
    )
  }
}