import React from 'react';

export default class Buttoms extends React.Component {


  render() {
    var numbers = ['MC','MR','MS','Mplus' ]
    var extras = [0, ',', '=', '+', '←', 'C', 'His']
    for (let i = 1; i < 10; i++) {
      numbers.push(i);
      switch (i) {
        case 3:
        numbers.push('/')
        numbers.id = '/'
          break;
        
        case 6:
        numbers.push('*')
        numbers.id = '*'      
          break;
        
        case 9:
        numbers.push('-')
        numbers.id = '-'        
          break;  

        default:
          break;
      }

    }

    extras.map((extra, i) =>
      numbers.push(extra)
    )

    return(
      <div className = 'row m-auto'>
      {
        numbers.map((number, i) => 
          <button onClick={()=> {this.props.handleClick(number)}} id={'buttom-' + number} type="submit" className="btn my-1 mx-auto p-1" >{number}</button>,
        )
      } 
      </div>
    )
  }
} 